from celery import Celery
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from random import choice


def make_celery(app):
    celery = Celery(app.import_name, backend=app.config['CELERY_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)

    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'amqp://localhost//'
app.config['CELERY_BACKEND'] = 'db+sqlite:///cel.db'
app.config['SQLALCHEMY_DATABASE_URI'] ='sqlite:///cel_data.db'

celery = make_celery(app)
db = SQLAlchemy(app)


class Results(db.Model):
    id = db.Column('id', db.Integer, primary_key=True)
    data = db.Column('data', db.String(128))


db.create_all()

@app.route('/process/<name>')
def process(name):
    reverse.delay(name)
    return 'I sent an async request'

@app.route('/insertdata')
def insertData():
    insert.delay()
    return 'I inserted a buch of shit'

@celery.task(name='main.reverse')
def reverse(name):
    return name[::-1]

@celery.task(name='main.insert')
def insert():
    for i in range(5000):
        data = ''.join(choice('ABCDE') for i in range(10))
        result = Results(data=data)

        db.session.add(result)
    db.session.commit()

    return 'Donezo'

if __name__ == '__main__':
    app.run(debug=True)
