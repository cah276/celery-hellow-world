from celery import Celery
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from random import choice


def make_celery(app):
    celery = Celery(app.import_name, backend=app.config['CELERY_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)

    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'amqp://localhost//'
app.config['CELERY_BACKEND'] = 'db+sqlite:///cel.db'
app.config['SQLALCHEMY_DATABASE_URI'] ='sqlite:///cel_data.db'

celery = make_celery(app)

celery.send_task("main.insert", kwargs=None)
